package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.repository.ICommandRepository;
import com.tsc.skuschenko.tm.constant.ArgumentConst;
import com.tsc.skuschenko.tm.constant.TerminalConst;
import com.tsc.skuschenko.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command ABOUT = new Command(
            TerminalConst.ABOUT,
            ArgumentConst.ABOUT,
            "show about information"
    );

    private static final Command HELP = new Command(
            TerminalConst.HELP,
            ArgumentConst.HELP,
            "show help information"
    );

    private static final Command VERSION = new Command(
            TerminalConst.VERSION,
            ArgumentConst.VERSION,
            "show version application"
    );

    private static final Command EXIT = new Command(
            TerminalConst.EXIT,
            null,
            "close program"
    );

    private static final Command INFO = new Command(
            TerminalConst.INFO,
            ArgumentConst.INFO,
            "close program"
    );

    private static final Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS,
            null,
            "show arguments"
    );

    private static final Command COMMANDS = new Command(
            TerminalConst.COMMANDS,
            null,
            "show commands"
    );

    private static final Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE,
            null,
            "create new task"
    );

    private static final Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR,
            null,
            "clear all tasks"
    );

    private static final Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST,
            null,
            "show all tasks"
    );

    private static final Command TASK_VIEW_BY_ID = new Command(
            TerminalConst.TASK_VIEW_BY_ID,
            null,
            "find task by id"
    );

    private static final Command TASK_VIEW_BY_INDEX = new Command(
            TerminalConst.TASK_VIEW_BY_INDEX,
            null,
            "find task by index"
    );

    private static final Command TASK_VIEW_BY_NAME = new Command(
            TerminalConst.TASK_VIEW_BY_NAME,
            null,
            "find task by name"
    );

    private static final Command TASK_REMOVE_BY_ID = new Command(
            TerminalConst.TASK_REMOVE_BY_ID,
            null,
            "remove task by id"
    );

    private static final Command TASK_REMOVE_BY_INDEX = new Command(
            TerminalConst.TASK_REMOVE_BY_INDEX,
            null,
            "remove task by index"
    );

    private static final Command TASK_REMOVE_BY_NAME = new Command(
            TerminalConst.TASK_REMOVE_BY_NAME,
            null,
            "remove task by name"
    );

    private static final Command TASK_UPDATE_BY_ID = new Command(
            TerminalConst.TASK_UPDATE_BY_ID,
            null,
            "update task by id"
    );

    private static final Command TASK_UPDATE_BY_INDEX = new Command(
            TerminalConst.TASK_UPDATE_BY_INDEX,
            null,
            "update task by index"
    );

    private static final Command TASK_START_BY_ID = new Command(
            TerminalConst.TASK_START_BY_ID,
            null,
            "start task by id"
    );

    private static final Command TASK_START_BY_INDEX = new Command(
            TerminalConst.TASK_START_BY_INDEX,
            null,
            "start task by index"
    );

    private static final Command TASK_START_BY_NAME = new Command(
            TerminalConst.TASK_START_BY_NAME,
            null,
            "start task by name"
    );

    private static final Command TASK_FINISH_BY_ID = new Command(
            TerminalConst.TASK_FINISH_BY_ID,
            null,
            "finish task by id"
    );

    private static final Command TASK_FINISH_BY_INDEX = new Command(
            TerminalConst.TASK_FINISH_BY_INDEX,
            null,
            "finish task by index"
    );

    private static final Command TASK_FINISH_BY_NAME = new Command(
            TerminalConst.TASK_FINISH_BY_NAME,
            null,
            "finish task by name"
    );

    private static final Command TASK_CHANGE_STATUS_BY_ID = new Command(
            TerminalConst.TASK_CHANGE_STATUS_BY_ID,
            null,
            "change task by id"
    );

    private static final Command TASK_CHANGE_STATUS_BY_INDEX = new Command(
            TerminalConst.TASK_CHANGE_STATUS_BY_INDEX,
            null,
            "change task by index"
    );

    private static final Command TASK_CHANGE_STATUS_BY_NAME = new Command(
            TerminalConst.TASK_CHANGE_STATUS_BY_NAME,
            null,
            "change task by name"
    );

    private static final Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE,
            null,
            "create new project"
    );

    private static final Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR,
            null,
            "clear all projects"
    );

    private static final Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST,
            null,
            "show all projects"
    );

    private static final Command PROJECT_VIEW_BY_ID = new Command(
            TerminalConst.PROJECT_VIEW_BY_ID,
            null,
            "find project by id"
    );

    private static final Command PROJECT_VIEW_BY_INDEX = new Command(
            TerminalConst.PROJECT_VIEW_BY_INDEX,
            null,
            "find project by index"
    );

    private static final Command PROJECT_VIEW_BY_NAME = new Command(
            TerminalConst.PROJECT_VIEW_BY_NAME,
            null,
            "find project by name"
    );

    private static final Command PROJECT_REMOVE_BY_ID = new Command(
            TerminalConst.PROJECT_REMOVE_BY_ID,
            null,
            "remove project by id"
    );

    private static final Command PROJECT_REMOVE_BY_INDEX = new Command(
            TerminalConst.PROJECT_REMOVE_BY_INDEX,
            null,
            "remove project by index"
    );

    private static final Command PROJECT_REMOVE_BY_NAME = new Command(
            TerminalConst.PROJECT_REMOVE_BY_NAME,
            null,
            "remove project by name"
    );

    private static final Command PROJECT_UPDATE_BY_ID = new Command(
            TerminalConst.PROJECT_UPDATE_BY_ID,
            null,
            "update project by id"
    );

    private static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            TerminalConst.PROJECT_UPDATE_BY_INDEX,
            null,
            "update project by index"
    );

    private static final Command PROJECT_START_BY_ID = new Command(
            TerminalConst.PROJECT_START_BY_ID,
            null,
            "start project by id"
    );

    private static final Command PROJECT_START_BY_INDEX = new Command(
            TerminalConst.PROJECT_START_BY_INDEX,
            null,
            "start project by index"
    );

    private static final Command PROJECT_START_BY_NAME = new Command(
            TerminalConst.PROJECT_START_BY_NAME,
            null,
            "start project by name"
    );

    private static final Command PROJECT_FINISH_BY_ID = new Command(
            TerminalConst.PROJECT_FINISH_BY_ID,
            null,
            "finish project by id"
    );

    private static final Command PROJECT_FINISH_BY_INDEX = new Command(
            TerminalConst.PROJECT_FINISH_BY_INDEX,
            null,
            "finish project by index"
    );

    private static final Command PROJECT_FINISH_BY_NAME = new Command(
            TerminalConst.PROJECT_FINISH_BY_NAME,
            null,
            "finish project by name"
    );

    private static final Command PROJECT_CHANGE_STATUS_BY_ID = new Command(
            TerminalConst.PROJECT_CHANGE_STATUS_BY_ID,
            null,
            "change project by id"
    );

    private static final Command PROJECT_CHANGE_STATUS_BY_INDEX = new Command(
            TerminalConst.PROJECT_CHANGE_STATUS_BY_INDEX,
            null,
            "change project by index"
    );

    private static final Command PROJECT_CHANGE_STATUS_BY_NAME = new Command(
            TerminalConst.PROJECT_CHANGE_STATUS_BY_NAME,
            null,
            "change project by name"
    );

    public static final Command[] TERMINAL_COMMANDS = new Command[]{
            ABOUT, HELP, VERSION, INFO, ARGUMENTS, COMMANDS, TASK_CREATE,
            TASK_CLEAR, TASK_LIST, TASK_VIEW_BY_ID, TASK_VIEW_BY_INDEX,
            TASK_VIEW_BY_NAME, TASK_REMOVE_BY_ID, TASK_REMOVE_BY_INDEX,
            TASK_REMOVE_BY_NAME, TASK_UPDATE_BY_ID, TASK_UPDATE_BY_INDEX,
            TASK_START_BY_ID, TASK_START_BY_INDEX, TASK_START_BY_NAME,
            TASK_FINISH_BY_ID, TASK_FINISH_BY_INDEX, TASK_FINISH_BY_NAME,
            TASK_CHANGE_STATUS_BY_ID, TASK_CHANGE_STATUS_BY_INDEX,
            TASK_CHANGE_STATUS_BY_NAME, PROJECT_VIEW_BY_ID,
            PROJECT_VIEW_BY_INDEX, PROJECT_VIEW_BY_NAME,
            PROJECT_REMOVE_BY_ID, PROJECT_REMOVE_BY_INDEX,
            PROJECT_REMOVE_BY_NAME, PROJECT_UPDATE_BY_ID,
            PROJECT_UPDATE_BY_INDEX, PROJECT_CREATE, PROJECT_CLEAR,
            PROJECT_LIST, PROJECT_START_BY_ID, PROJECT_START_BY_INDEX,
            PROJECT_START_BY_NAME, PROJECT_FINISH_BY_ID,
            PROJECT_FINISH_BY_INDEX, PROJECT_FINISH_BY_NAME,
            PROJECT_CHANGE_STATUS_BY_ID, PROJECT_CHANGE_STATUS_BY_INDEX,
            PROJECT_CHANGE_STATUS_BY_NAME, EXIT
    };

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
