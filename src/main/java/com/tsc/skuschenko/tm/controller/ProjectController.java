package com.tsc.skuschenko.tm.controller;

import com.tsc.skuschenko.tm.api.controller.IProjectController;
import com.tsc.skuschenko.tm.api.service.IProjectService;
import com.tsc.skuschenko.tm.constant.InformationConst;
import com.tsc.skuschenko.tm.constant.TerminalConst;
import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showProjects() {
        showOperationInfo(TerminalConst.PROJECT_LIST);
        final List<Project> projects = projectService.findAll();
        int index = 1;
        for (final Project project : projects) {
            System.out.println(index + ". " + project);
            index++;
        }
        showOperationInfo(InformationConst.OPERATION_OK);
    }

    @Override
    public void create() {
        showOperationInfo(TerminalConst.PROJECT_CREATE);
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.add(name, description);
        if (project == null) {
            showOperationInfo(InformationConst.OPERATION_FAIL);
            return;
        }
        showProject(project);
        showOperationInfo(InformationConst.OPERATION_OK);
    }

    @Override
    public void clear() {
        showOperationInfo(TerminalConst.PROJECT_CLEAR);
        projectService.clear();
        showOperationInfo(InformationConst.OPERATION_OK);
    }

    @Override
    public void startProjectById() {
        showOperationInfo(TerminalConst.PROJECT_START_BY_ID);
        System.out.println("ENTER ID:");
        final String value = TerminalUtil.nextLine();
        final Project project = projectService.startProjectById(value);
        if (project == null) {
            showOperationInfo(InformationConst.OPERATION_FAIL);
            return;
        }
        showOperationInfo(InformationConst.OPERATION_OK);
    }

    @Override
    public void startProjectByIndex() {
        showOperationInfo(TerminalConst.PROJECT_START_BY_INDEX);
        System.out.println("ENTER INDEX:");
        final Integer value = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.startProjectByIndex(value);
        if (project == null) {
            showOperationInfo(InformationConst.OPERATION_FAIL);
            return;
        }
        showProject(project);
        showOperationInfo(InformationConst.OPERATION_OK);
    }

    @Override
    public void startProjectByName() {
        showOperationInfo(TerminalConst.PROJECT_START_BY_NAME);
        System.out.println("ENTER NAME:");
        final String value = TerminalUtil.nextLine();
        final Project project = projectService.startProjectByName(value);
        if (project == null) {
            showOperationInfo(InformationConst.OPERATION_FAIL);
            return;
        }
        showProject(project);
        showOperationInfo(InformationConst.OPERATION_OK);
    }

    @Override
    public void completeProjectById() {
        showOperationInfo(TerminalConst.PROJECT_FINISH_BY_ID);
        System.out.println("ENTER ID:");
        final String value = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(value);
        if (project == null) {
            showOperationInfo(InformationConst.OPERATION_FAIL);
            return;
        }
        showOperationInfo(InformationConst.OPERATION_OK);
    }

    @Override
    public void completeProjectByIndex() {
        showOperationInfo(TerminalConst.PROJECT_FINISH_BY_INDEX);
        System.out.println("ENTER INDEX:");
        final Integer value = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(value);
        if (project == null) {
            showOperationInfo(InformationConst.OPERATION_FAIL);
            return;
        }
        showProject(project);
        showOperationInfo(InformationConst.OPERATION_OK);
    }

    @Override
    public void completeProjectByName() {
        showOperationInfo(TerminalConst.PROJECT_FINISH_BY_NAME);
        System.out.println("ENTER NAME:");
        final String value = TerminalUtil.nextLine();
        final Project project = projectService.completeProjectByName(value);
        if (project == null) {
            showOperationInfo(InformationConst.OPERATION_FAIL);
            return;
        }
        showProject(project);
        showOperationInfo(InformationConst.OPERATION_OK);

    }

    @Override
    public void changeProjectStatusById() {
        showOperationInfo(TerminalConst.PROJECT_CHANGE_STATUS_BY_ID);
        System.out.println("ENTER ID:");
        final String valueId = TerminalUtil.nextLine();
        Project project = projectService.findOneById(valueId);
        if (project == null) {
            showOperationInfo(InformationConst.OPERATION_FAIL);
            return;
        }
        project = projectService.changeProjectStatusById(
                valueId, readProjectStatus()
        );
        if (project == null) {
            showOperationInfo(InformationConst.OPERATION_FAIL);
            return;
        }
        showProject(project);
        showOperationInfo(InformationConst.OPERATION_OK);
    }

    @Override
    public void changeProjectStatusByIndex() {
        showOperationInfo(TerminalConst.PROJECT_CHANGE_STATUS_BY_INDEX);
        System.out.println("ENTER INDEX:");
        final Integer valueIndex = TerminalUtil.nextNumber() - 1;
        Project project = projectService.findOneByIndex(valueIndex);
        if (project == null) {
            showOperationInfo(InformationConst.OPERATION_FAIL);
            return;
        }
        project = projectService.changeProjectStatusByIndex(
                valueIndex, readProjectStatus()
        );
        if (project == null) {
            showOperationInfo(InformationConst.OPERATION_FAIL);
            return;
        }
        showProject(project);
        showOperationInfo(InformationConst.OPERATION_OK);
    }

    @Override
    public void changeProjectStatusByName() {
        showOperationInfo(TerminalConst.PROJECT_CHANGE_STATUS_BY_NAME);
        System.out.println("ENTER NAME:");
        final String value = TerminalUtil.nextLine();
        Project project = projectService.findOneByName(value);
        if (project == null) {
            showOperationInfo(InformationConst.OPERATION_FAIL);
            return;
        }
        project = projectService.changeProjectStatusByName(
                value, readProjectStatus()
        );
        if (project == null) {
            showOperationInfo(InformationConst.OPERATION_FAIL);
            return;
        }
        showProject(project);
        showOperationInfo(InformationConst.OPERATION_OK);
    }

    @Override
    public void showProjectById() {
        showOperationInfo(TerminalConst.PROJECT_VIEW_BY_ID);
        System.out.println("ENTER ID:");
        final String value = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(value);
        if (project == null) {
            showOperationInfo(InformationConst.OPERATION_FAIL);
            return;
        }
        showOperationInfo(InformationConst.OPERATION_OK);
    }

    @Override
    public void showProjectByIndex() {
        showOperationInfo(TerminalConst.PROJECT_VIEW_BY_INDEX);
        System.out.println("ENTER INDEX:");
        final Integer value = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findOneByIndex(value);
        if (project == null) {
            showOperationInfo(InformationConst.OPERATION_FAIL);
            return;
        }
        showProject(project);
        showOperationInfo(InformationConst.OPERATION_OK);
    }

    @Override
    public void showProjectByName() {
        showOperationInfo(TerminalConst.PROJECT_VIEW_BY_NAME);
        System.out.println("ENTER NAME:");
        final String value = TerminalUtil.nextLine();
        final Project project = projectService.findOneByName(value);
        if (project == null) {
            showOperationInfo(InformationConst.OPERATION_FAIL);
            return;
        }
        showProject(project);
        showOperationInfo(InformationConst.OPERATION_OK);
    }

    @Override
    public void removeProjectById() {
        showOperationInfo(TerminalConst.PROJECT_REMOVE_BY_ID);
        System.out.println("ENTER ID:");
        final String value = TerminalUtil.nextLine();
        final Project project = projectService.removeOneById(value);
        if (project == null) {
            showOperationInfo(InformationConst.OPERATION_FAIL);
            return;
        }
        showOperationInfo(InformationConst.OPERATION_OK);
    }

    @Override
    public void removeProjectByIndex() {
        showOperationInfo(TerminalConst.PROJECT_REMOVE_BY_INDEX);
        System.out.println("ENTER INDEX:");
        final Integer value = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.removeOneByIndex(value);
        if (project == null) {
            showOperationInfo(InformationConst.OPERATION_FAIL);
            return;
        }
        showOperationInfo(InformationConst.OPERATION_OK);
    }

    @Override
    public void removeProjectByName() {
        showOperationInfo(TerminalConst.PROJECT_REMOVE_BY_NAME);
        System.out.println("ENTER NAME:");
        final String value = TerminalUtil.nextLine();
        final Project project = projectService.removeOneByName(value);
        if (project == null) {
            showOperationInfo(InformationConst.OPERATION_FAIL);
            return;
        }
        showOperationInfo(InformationConst.OPERATION_OK);
    }

    @Override
    public void updateProjectById() {
        showOperationInfo(TerminalConst.PROJECT_UPDATE_BY_ID);
        System.out.println("ENTER ID:");
        final String valueId = TerminalUtil.nextLine();
        Project project = projectService.findOneById(valueId);
        if (project == null) {
            showOperationInfo(InformationConst.OPERATION_FAIL);
            return;
        }
        System.out.println("ENTER NAME:");
        final String valueName = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String valueDescription = TerminalUtil.nextLine();
        project = projectService.updateOneById(
                valueId,
                valueName,
                valueDescription);
        if (project == null) {
            showOperationInfo(InformationConst.OPERATION_FAIL);
            return;
        }
        showProject(project);
        showOperationInfo(InformationConst.OPERATION_OK);
    }

    @Override
    public void updateProjectByIndex() {
        showOperationInfo(TerminalConst.PROJECT_UPDATE_BY_INDEX);
        System.out.println("ENTER INDEX:");
        final Integer valueIndex = TerminalUtil.nextNumber() - 1;
        Project project = projectService.findOneByIndex(valueIndex);
        if (project == null) {
            showOperationInfo(InformationConst.OPERATION_FAIL);
            return;
        }
        System.out.println("ENTER NAME:");
        final String valueName = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String valueDescription = TerminalUtil.nextLine();
        project = projectService.updateOneByIndex(
                valueIndex,
                valueName,
                valueDescription);
        if (project == null) {
            showOperationInfo(InformationConst.OPERATION_FAIL);
            return;
        }
        showProject(project);
        showOperationInfo(InformationConst.OPERATION_OK);
    }

    private void showProject(final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + project.getStatus().getDisplayName());
    }

    private void showOperationInfo(final String info) {
        System.out.println("[" + info.toUpperCase() + "]");
    }

    private Status readProjectStatus() {
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        return status;
    }

}