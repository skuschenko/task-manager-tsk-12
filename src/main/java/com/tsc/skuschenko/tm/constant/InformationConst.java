package com.tsc.skuschenko.tm.constant;

public interface InformationConst {

    String OPERATION_OK = "ok";

    String OPERATION_FAIL = "fail";

}
