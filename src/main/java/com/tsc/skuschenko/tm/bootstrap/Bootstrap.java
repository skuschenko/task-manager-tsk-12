package com.tsc.skuschenko.tm.bootstrap;

import com.tsc.skuschenko.tm.api.controller.ICommandController;
import com.tsc.skuschenko.tm.api.controller.IProjectController;
import com.tsc.skuschenko.tm.api.controller.ITaskController;
import com.tsc.skuschenko.tm.api.repository.ICommandRepository;
import com.tsc.skuschenko.tm.api.repository.IProjectRepository;
import com.tsc.skuschenko.tm.api.repository.ITaskRepository;
import com.tsc.skuschenko.tm.api.service.ICommandService;
import com.tsc.skuschenko.tm.api.service.IProjectService;
import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.constant.ArgumentConst;
import com.tsc.skuschenko.tm.constant.TerminalConst;
import com.tsc.skuschenko.tm.controller.CommandController;
import com.tsc.skuschenko.tm.controller.ProjectController;
import com.tsc.skuschenko.tm.controller.TaskController;
import com.tsc.skuschenko.tm.repository.CommandRepository;
import com.tsc.skuschenko.tm.repository.ProjectRepository;
import com.tsc.skuschenko.tm.repository.TaskRepository;
import com.tsc.skuschenko.tm.service.CommandService;
import com.tsc.skuschenko.tm.service.ProjectService;
import com.tsc.skuschenko.tm.service.TaskService;
import com.tsc.skuschenko.tm.util.TerminalUtil;

public class Bootstrap {

    private final ICommandRepository commandRepository
            = new CommandRepository();

    private final ICommandService commandService
            = new CommandService(commandRepository);

    private final ICommandController commandController
            = new CommandController(commandService);

    private final ITaskRepository taskRepository
            = new TaskRepository();

    private final ITaskService taskService
            = new TaskService(taskRepository);

    private final ITaskController taskController
            = new TaskController(taskService);

    private final IProjectRepository projectRepository
            = new ProjectRepository();

    private final IProjectService projectService
            = new ProjectService(projectRepository);

    private final IProjectController projectController
            = new ProjectController(projectService);

    public void run(final String... args) {
        System.out.println("***Welcome to task manager***");
        if (parseArgs(args)) commandController.exit();
        while (true) {
            System.out.print("Enter command:");
            final String command = TerminalUtil.nextLine();
            parseCommand(command);
        }
    }

    public void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg.toLowerCase()) {
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showSystemInfo();
                break;
            default:
                commandController.showWrongArg(arg);
                break;
        }
    }

    public void parseCommand(final String command) {
        if (command == null) return;
        switch (command.toLowerCase()) {
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.EXIT:
                commandController.exit();
                break;
            case TerminalConst.INFO:
                commandController.showSystemInfo();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArgs();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTasks();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.create();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clear();
                break;
            case TerminalConst.TASK_VIEW_BY_ID:
                taskController.showTaskById();
                break;
            case TerminalConst.TASK_VIEW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case TerminalConst.TASK_VIEW_BY_NAME:
                taskController.showTaskByName();
                break;
            case TerminalConst.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case TerminalConst.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case TerminalConst.TASK_REMOVE_BY_NAME:
                taskController.removeTaskByName();
                break;
            case TerminalConst.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case TerminalConst.TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case TerminalConst.TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case TerminalConst.TASK_START_BY_NAME:
                taskController.startTaskByName();
                break;
            case TerminalConst.TASK_FINISH_BY_ID:
                taskController.completeTaskById();
                break;
            case TerminalConst.TASK_FINISH_BY_INDEX:
                taskController.completeTaskByIndex();
                break;
            case TerminalConst.TASK_FINISH_BY_NAME:
                taskController.completeTaskByName();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_NAME:
                taskController.changeTaskStatusByName();
                break;
            case TerminalConst.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.create();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clear();
                break;
            case TerminalConst.PROJECT_VIEW_BY_ID:
                projectController.showProjectById();
                break;
            case TerminalConst.PROJECT_VIEW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case TerminalConst.PROJECT_VIEW_BY_NAME:
                projectController.showProjectByName();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_NAME:
                projectController.removeProjectByName();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case TerminalConst.PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case TerminalConst.PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case TerminalConst.PROJECT_START_BY_NAME:
                projectController.startProjectByName();
                break;
            case TerminalConst.PROJECT_FINISH_BY_ID:
                projectController.completeProjectById();
                break;
            case TerminalConst.PROJECT_FINISH_BY_INDEX:
                projectController.completeProjectByIndex();
                break;
            case TerminalConst.PROJECT_FINISH_BY_NAME:
                projectController.completeProjectByName();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_NAME:
                projectController.changeProjectStatusByName();
                break;
            default:
                commandController.showWrongCmd(command);
                break;
        }
    }

    public boolean parseArgs(String[] args) {
        if (args == null || args.length < 1) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

}
